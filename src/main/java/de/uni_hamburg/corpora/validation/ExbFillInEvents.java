/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package de.uni_hamburg.corpora.validation;

import de.uni_hamburg.corpora.*;
import de.uni_hamburg.corpora.utilities.TypeConverter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import org.jdom.Document;
import org.jdom.Element;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;
import org.xml.sax.SAXException;


/**
 *
 * @author bay7303
 */

public class ExbFillInEvents extends Checker implements CorpusFunction {

    String newEventTier = null;
    String newGloss = null;
    String lookupTier = null;
    String lookupPrefix = null;
    String lookupValue = null;
    String lookupSuffix = null;
    String contextValue = null;
    String contextTier = null;
    String contextPrefix = null;
    String contextSuffix = null;
    
    public void setNewEventTier(String tier) {
        newEventTier = tier;
    }
    
    public void setNewValue(String gloss) {
        newGloss = gloss;
    }
    
    public void setReplacementTier(String tier) {
        lookupTier = tier;
    }
    
    public void setReplacementPrefix (String prefix) {
        lookupPrefix = prefix;
    }
    
    public void setOriginalValue(String gloss) {
        lookupValue = gloss;
    }
    
    public void setReplacementSuffix (String suffix) {
        lookupSuffix = suffix;
    }
            
    public void setContextValue(String gloss) {
        contextValue = gloss;
    }
    
    public void setContextTier(String tier) {
        contextTier = tier;
    }
    
    public void setContextPrefix (String prefix) {
        contextPrefix = prefix;
    }
    
    public void setContextSuffix (String suffix) {
        contextSuffix = suffix;
    }
        
    Document doc;
    //XMLOutputter xmOut = new XMLOutputter(); //for testing

    public ExbFillInEvents() {
        //fixing option available
        super(true);
    }
        
    /**
     * Description will be here
     */
    
    @Override
    public Report function(CorpusData cd, Boolean fix) 
            throws SAXException, IOException, ParserConfigurationException, URISyntaxException, JDOMException, TransformerException, XPathExpressionException {
        Report stats = new Report();      
        doc = TypeConverter.String2JdomDocument(cd.toSaveableString());
        String patternRegex = lookupPrefix + lookupValue + lookupSuffix;
        Pattern glossFinder = Pattern.compile(patternRegex);
        //System.out.println(glossFinder.toString());
        CorpusIO cio = new CorpusIO();        
        String XPathGloss = "//tier[@category='" + lookupTier + "']/event";
        XPath XGloss = XPath.newInstance(XPathGloss);
        List allGlosses = XGloss.selectNodes(doc);
        System.out.println(cd.getFilename());
        for (int g = 0; g < allGlosses.size(); g++) {
            Object og = allGlosses.get(g);
            if (og instanceof Element) {
                Element matchedElement = (Element) og;
                String glossText = matchedElement.getText();
                //System.out.println(glossText + "is the gloss text I've found");
                //System.out.println(glossFinder.toString() + "is the regular expression");
                Matcher m = glossFinder.matcher(glossText);
                if (m.find()) {
                    if (contextValue != null) {
                        //System.out.println("We have a match! Let's look for context... which should be " + contextValue);
                        String timeStamp = matchedElement.getAttributeValue("start");
                        String timeEnd = matchedElement.getAttributeValue("end");
                        Element parentTier = matchedElement.getParentElement();
                        String speaker = parentTier.getAttributeValue("speaker");
                        String XPathContext = "//tier[@category='" + contextTier + "'][@speaker='" + speaker + "']/event[@start='" + timeStamp + "']";
                        XPath XContext = XPath.newInstance(XPathContext);
                        Object co = XContext.selectSingleNode(doc);
                        Element ce = (Element) co;
                        String contextText = ce.getText();
                        String contextRegex = contextPrefix + contextValue + contextSuffix;
                        Pattern contextFinder = Pattern.compile(contextRegex);
                        //System.out.println(contextText + " is the context value");
                        //System.out.println(contextFinder.toString());
                        Matcher ctxm = contextFinder.matcher(contextText);
                        if (ctxm.find()) {
                            System.out.println(m.group() + " matched an expression you wanted to modify in " + glossText + " The context is " + contextText);
                            String XPathCheckEvent = "//tier[@category='" + newEventTier + "'][@speaker='" + speaker + "']/event[@start='" + timeStamp + "']";
                            XPath XEvent = XPath.newInstance(XPathCheckEvent);
                            try {
                                Object eo = XEvent.selectSingleNode(doc);
                                Element ee = (Element) eo;
                                System.out.println(ee.getText() + " already exists at " + timeStamp + ", I'll add more...");
                                String curText = ee.getText();
                                ee.setText(curText + " " + newGloss);
                            } catch (Exception e) {
                                String XPathNewEvent = "//tier[@category='" + newEventTier + "'][@speaker='" + speaker + "']";
                                XPath XNewEvent = XPath.newInstance(XPathNewEvent);
                                Object neo = XNewEvent.selectSingleNode(doc);
                                Element eeo = (Element) neo;
                                Element newEvent = new Element ("event");
                                newEvent.setAttribute("start", timeStamp);
                                newEvent.setAttribute("end", timeEnd);
                                newEvent.setText(newGloss);
                                eeo.addContent(newEvent);
                            }
                            //glossText = m.replaceAll(newGloss);
                            //matchedElement.setText(glossText);
                            //System.out.println(matchedElement.getText());
                            if (fix) {
                                cd.updateUnformattedString(TypeConverter.JdomDocument2String(doc));
                                cio.write(cd, cd.getURL());
                                stats.addFix(function, cd, "Added " + newGloss + ", tier is " + newEventTier + ", speaker is " + speaker + ". The lookup value is " + glossText + " at " + timeStamp);
                            }
                        } else {System.out.println("The context didn't match :(");}
                    } else {
                        String timeStamp = matchedElement.getAttributeValue("start");
                        String timeEnd = matchedElement.getAttributeValue("end");
                        Element parentTier = matchedElement.getParentElement();
                        String speaker = parentTier.getAttributeValue("speaker");
                        System.out.println(m.group() + " matched an expression you wanted to modify in " + glossText + " at " + timeStamp);
                        String XPathCheckEvent = "//tier[@category='" + newEventTier + "'][@speaker='" + speaker + "']/event[@start='" + timeStamp + "']";
                        XPath XEvent = XPath.newInstance(XPathCheckEvent);
                        try {
                            Object eo = XEvent.selectSingleNode(doc);
                            Element ee = (Element) eo;
                            //System.out.println(ee.getText() + " already exists at " + timeStamp + ",skipping...");
                            String curText = ee.getText();
                            ee.setText(curText + " " + newGloss);
                        } catch (Exception e) {
                            String XPathNewEvent = "//tier[@category='" + newEventTier + "'][@speaker='" + speaker + "']";
                            XPath XNewEvent = XPath.newInstance(XPathNewEvent);
                            Object neo = XNewEvent.selectSingleNode(doc);
                            Element eeo = (Element) neo;
                            System.out.println(eeo);
                            Element newEvent = new Element ("event");
                            newEvent.setAttribute("start", timeStamp);
                            newEvent.setAttribute("end", timeEnd);
                            newEvent.setText(newGloss);
                            eeo.addContent(newEvent);
                        }
                        //glossText = m.replaceAll(newGloss);
                        //matchedElement.setText(glossText);
                        //System.out.println(matchedElement.getText());
                        if (fix) {
                            cd.updateUnformattedString(TypeConverter.JdomDocument2String(doc));
                            cio.write(cd, cd.getURL());
                            stats.addFix(function, cd, "Added " + newGloss + ", tier is " + newEventTier + ", speaker is " + speaker + ". The lookup value is " + glossText + " at " + timeStamp);
                        }
                    }
                }
            }
        }
        
        return stats;
    }
    
    /**
     * Default function which determines for what type of files (basic
     * transcription, segmented transcription, coma etc.) this feature can be
     * used.
     */
    @Override
    public Collection<Class<? extends CorpusData>> getIsUsableFor()  {
        return Collections.singleton(EXMARaLDACorpusData.class);
    }

    /**
     * Default function which returns a two/three line description of what this
     * class is about.
     */
    @Override
    public String getDescription() {
        String description = "This class replaces glosses in EXBs";
        return description;
    }

    @Override
    public Report function(Corpus c, Boolean fix) throws SAXException, IOException, ParserConfigurationException, URISyntaxException, JDOMException, TransformerException, XPathExpressionException {
        Report stats = new Report();
        for (CorpusData cdata : c.getBasicTranscriptionData()) {
            stats.merge(function(cdata, fix));
        }
        return stats;
    }
}